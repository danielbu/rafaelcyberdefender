const axios = require('axios');
const router = require('express').Router();
let AttackPattern = require('./attackPattern.model');
const FormData = require('form-data')

//a route that gets all of the attack patterns from the database
router.route('/').get((req, res) => {
    AttackPattern.find()
      .then(attackPatterns => res.json(attackPatterns))
      .catch(err => res.status(400).json('Error: ' + err));
});

//a route that gets all of the attack patterns filtered by a specific property 
router.route('/filter').post((req, res) => {
  const searchValue = req.body.searchValue;
  const regex = new RegExp("\w*(" + searchValue + ")\w*", "i");

  if (req.body.searchBy == "name"){
    AttackPattern.find({name: regex})
    .then(attackPatterns => res.json(attackPatterns))
    .catch(err => res.status(400).json('Error: ' + err));
  }
  else if (req.body.searchBy == "description"){
    AttackPattern.find({description: regex})
    .then(attackPatterns => res.json(attackPatterns))
    .catch(err => res.status(400).json('Error: ' + err));
  }
  else if (req.body.searchBy == "x_mitre_detection" || req.body.searchBy == "detection"){
    AttackPattern.find({x_mitre_detection: regex})
    .then(attackPatterns => res.json(attackPatterns))
    .catch(err => res.status(400).json('Error: ' + err));
  }
});

//a route that check a file or a hash in virustotal
router.route('/vt-check').post((req, res) => {
  var apiKey = process.env.VT_API_KEY; //get the api key from the .env file
  var checkProperty = req.body.checkProperty;
  if (checkProperty === "hash")
    checkProperty = "file";
  var apiUrl = `https://www.virustotal.com/vtapi/v2/${checkProperty}/report`;
  var checkValue = req.body.checkValue;

  axios.get(apiUrl, {
      params: {
          'apikey': apiKey,
          'resource': checkValue
      }
  })
  .then(response => res.json(response.data))
  .catch(err => res.json(err))
});

//a route that handles all of the cuckoo get requests
router.route('/cuckoo-get').post((req, res) => {
  var apiKey = process.env.CUCKOO_API_KEY;
  var service = req.body.service;
  var apiUrl = `http://192.168.1.26:8090/${service}`;

  axios.get(apiUrl, {
    headers: {
      'Authorization': `Bearer ${apiKey}`
    }
  })
  .then(response => res.json(response.data))
  .catch(err => res.json(err))
});

//a route that handles all of the cuckoo post requests
router.route('/cuckoo-post').post((req, res) => {
  var apiKey = process.env.CUCKOO_API_KEY;
  var service = req.body.service;
  var apiUrl = `http://192.168.1.26:8090/${service}`;

  var formData = new FormData();
  formData.append("url", "google.com");

  axios.post(apiUrl, formData, {
    headers: {
      Authorization: `Bearer ${apiKey}`
    }
  })
  .then(response => res.json(response.data))
  .catch(err => res.json(err))

});

module.exports = router;