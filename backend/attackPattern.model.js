const mongoose = require('mongoose');

const Schema = mongoose.Schema;

//create the attack pattern schema
const attack_patternSchema = new Schema({
    name: { type: String },
    description: { type: String },
    id: { type: String, unique: true },
    x_mitre_platforms: { type: Array },
    x_mitre_detection: { type: String },
    phase_name: { type: String },
});

const attack_pattern = mongoose.model('attack_pattern', attack_patternSchema);

module.exports = attack_pattern;