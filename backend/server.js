//import dependencies
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

//initialize app
const app = express();
const port = process.env.PORT || 5000; //set port
app.use(cors());
app.use(express.json()); //allows to parse JSON

//ATLAS_URI is the connection string to MongoDB database (initialized in .env for security reasons)
const uri = process.env.ATLAS_URI;

//set connection with MongoDB database
//add (useNewUrlParser, useCreateIndex, useUnifiedTopology) to deal with MongoDB updates and changes
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true})
    //if connected to database start listening to port
    .then(() => app.listen(port, () => console.log(`Connection with MongoDB established successfully\nServer is running on port: ${port}`)))
    //catch possible errors
    .catch((error) => console.log(`Couldn't establish connection with MongoDB: ${error.message}`));

//to avoid warnings in the console
mongoose.set('useFindAndModify', false);

//set the route
const attackPatternRouter = require('./attackPatternsRoute');
app.use('/attackPatterns', attackPatternRouter);



//a one time use code 
//the purpose of it is to load all of the data to the data base

// const fs = require('fs').promises;
// let attack_pattern = require("../rafael-backend/attackPattern.model");

// async function main() {
//     await saveFiles();
// }

// main();

// async function saveFiles() {
//     //save all of the attack patterns file from the atack-pattern directory
//     const attackPatternFiles = await fs.readdir("../attack-pattern");

//     for (i = 0; i < attackPatternFiles.length; i++){
//         //iterate over the files
//         const fileObjects = require(`../attack-pattern/${attackPatternFiles[i]}`).objects[0];

//         //save all of the wanted properties from the file
//         let name = fileObjects.name;
//         let description = fileObjects.description;
//         let id = fileObjects.id;
//         let x_mitre_platforms = fileObjects.x_mitre_platforms;
//         let x_mitre_detection = fileObjects.x_mitre_detection;
//         if (fileObjects.kill_chain_phases != undefined){
//             phase_name = fileObjects.kill_chain_phases[0].phase_name;
//         }
//         else{
//             phase_name = "NA";
//         }

//         //if the property is missing set it to 'NA'
//         if (description == undefined){
//             description = "NA";
//         }
//         if (x_mitre_platforms == undefined){
//             x_mitre_platforms = "NA";
//         }
//         if (x_mitre_detection == undefined){
//             x_mitre_detection = "NA";
//         }

//         //create a new attack_pattern object
//         let newAttackPattern = new attack_pattern({
//             name, 
//             description, 
//             id, 
//             x_mitre_platforms, 
//             x_mitre_detection, 
//             phase_name,
//         });

//         //save the attack_pattern object in the data base
//         newAttackPattern.save()
//             .then(() => console.log(i))
//             .catch((error) => console.log(`Error: ${error.message}`));
//     }
// }