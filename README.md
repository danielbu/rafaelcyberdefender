# Cyber Defender
A website built with MERN stack:  
    - MongoDB as the database  
    - Express.js as the web framework  
    - React.js as the client side JavaScript framework  
    - Node.js as the server side  
  
More on the process of creation in DeveloperLog.docx

## Example of use
[![](https://i9.ytimg.com/vi_webp/YVLlaNFwM6c/mqdefault.webp?v=6080881e&sqp=CPjkxJAG&rs=AOn4CLA1tCWO7QGV3tDvBcL19BmCxrNC_g)](https://www.youtube.com/watch?v=YVLlaNFwM6c)
