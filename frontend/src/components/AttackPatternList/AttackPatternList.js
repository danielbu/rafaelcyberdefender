import React, { Component, useState } from "react"
import "bootstrap/dist/css/bootstrap.min.css"
import axios from 'axios'
import InfoWindow from "../InfoWindow/InfoWindow"
import "./AttackPatternList.css"

//the AttackPattern component
//implemented as a functional react component
function AttackPattern(props) {
    const [isOpen, setIsOpen] = useState(false);
    
    return (
        <tr>
            <td>{props.ap.name}</td>
            <td>{props.ap.description.substring(0, 150) + (props.ap.description.length > 150 ? "..." : "")}</td>
            <td>{props.ap.x_mitre_detection.substring(0, 150) + (props.ap.x_mitre_detection.length > 150 ? "..." : "")}</td>
            <td className="info-btn-td"><button className="btn btn-light" onClick={() => setIsOpen(true)}>Show more</button></td>
            <InfoWindow open={isOpen} info={props.ap} onClose={() => setIsOpen(false)}/>
        </tr>
    )
}

//the AttackPatternList component
//implemented as a class component
export default class AttackPatternList extends Component {
    constructor(props) {
        //super() is used in JS classes when defining the constructor of a sub-class
        super(props);
        //init the state of the component (the attackPatterns array)
        this.state = {
            attackPatterns: [],
            searchTerm: "",
            searchBy: "description",
            isInfoOpen: false,
        };
    }

    //get all of the attackPatterns from the data base
    //react will call this function(lifecycle method) right before anything displays on the page
    componentDidMount() {
        axios.get('http://localhost:5000/attackPatterns/')
            .then(response => {
                this.setState({attackPatterns: response.data})
            })
            .catch((error) => {
                console.log(error);
            })
    }

    //return the attackPatterns as rows in a table
    attackPatternList() {
        return this.state.attackPatterns.filter((currAttackPattern) => {
            if (this.state.searchTerm === ""){
                return currAttackPattern;
            } else if (currAttackPattern[this.state.searchBy].toLowerCase().includes(this.state.searchTerm.toLowerCase())){
                return currAttackPattern;
            }
        }).map((currAttackPattern, i) => {
            return <AttackPattern key={i}  ap={currAttackPattern}/>
        })
    }

    render() {
        return (
        <div className="tableDiv">
            <div className="above-table">
                <label>Search  </label>
                <input type="text" placeholder="something" onChange={(event) => {
                    this.setState({searchTerm: event.target.value})
                }}/>
                <label>  by: </label>
                <select label="by: " id="searchBy" onChange={() => this.setState({searchBy: document.getElementById("searchBy").value})}>
                    <option value="name">name</option>
                    <option value="description" selected>description</option>
                    <option value="x_mitre_detection">detection</option>
                </select>
                {/* <h2 className="ap-header">Attack Patterns</h2> */}
            </div>
            <div className="scrollable">
            <table className="table table-dark table-hover table-bordered table-condensed">
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Detections</th>
                    <th>Info</th>
                    </tr>
                </thead>
                <tbody>
                    {this.attackPatternList()}
                </tbody>
            </table>
            </div>
        </div>
        )
    }
}
