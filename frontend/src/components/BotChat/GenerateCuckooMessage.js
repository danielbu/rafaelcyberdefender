import axios from 'axios'

async function listTasks(){
    var tasks = "Existing tasks:\n";
    const filter = {
        service: "tasks/list/20"
    }

    await axios.post('http://localhost:5000/attackPatterns/cuckoo-get', filter)
    .then(res => {
        if (res.data.tasks.length === 0)
            tasks = "No Existing tasks";

        for (var i = 0; i < res.data.tasks.length; i++){
            tasks += `\nTask ${res.data.tasks[i].id} is of category ${res.data.tasks[i].category}:\n${res.data.tasks[i].target}\n`;
            tasks += `Status: ${res.data.tasks[i].status}\nAdded on: ${res.data.tasks[i].added_on}\n`;
            if (res.data.tasks[i].status === "reported"){
                tasks += `Duration: ${res.data.tasks[i].duration} seconds\n`
            }
        }
    })

    return tasks;
}

async function deleteTask(message){
    var botMessage = "Task does not exist";
    const taskId = message.split(" ")[message.split(" ").length - 1];
    if (isNaN(taskId))
        return "Task id must be a number.";
    const filter = {
        service: `tasks/delete/${taskId}`
    }

    await axios.post('http://localhost:5000/attackPatterns/cuckoo-get', filter)
    .then(res => {
        if(res.data.status === "OK")
            botMessage = "Task deleted successfully";
    })

    return botMessage;
}

async function reportTask(message){
    const taskId = message.split(" ")[message.split(" ").length - 1];
    if (isNaN(taskId))
        return "Task id must be a number.";
    const filter = {
        service: `tasks/report/${taskId}/html`
    }

    await axios.post('http://localhost:5000/attackPatterns/cuckoo-get', filter)
    .then(res => {
        if (res.data.name === undefined)
            document.write("<button style=\"width: 100%; height: 6%; background-color: #343a40; color: white;\"" +
             "onClick=\"window.location.href='http://localhost:3000';\">Back To Site</button>" + res.data);
    })
}

async function createUrlTask(message){
    var botMessage = "Unknown url";
    console.log(message.split(" ")[message.split(" ").length - 1]);
    const filter = {
        service: 'tasks/create/url',
        url : message.split(" ")[message.split(" ").length - 1]
    }

    await axios.post('http://localhost:5000/attackPatterns/cuckoo-post', filter)
    .then(res => {
        console.log(res.data);
        if (res.data.task_id !== undefined && res.data.task_id !== null)
            botMessage = `Successfully created a new task with id ${res.data.task_id}`;
    })

    return botMessage;
}

export function generateCuckooMessage(message){
    if (message.split("cuckoo ")[1] == undefined)
        return "You need to specify a cuckoo function.\nSay 'help' for list of commands.";
    //ignore the first part of the message
    const msg = message.split("cuckoo ")[1].toLowerCase();

    if (msg.includes("list")){
        return listTasks();
    }
    else if (msg.includes("delete")){
        return deleteTask(message);
    }
    else if (msg.includes("report")){
        return reportTask(message);
    }
    else if (msg.includes("create") && msg.includes("url")){
        return createUrlTask(message);
    }

    return "Unknown cuckoo function.\nSay 'help' for list of commands.";
}