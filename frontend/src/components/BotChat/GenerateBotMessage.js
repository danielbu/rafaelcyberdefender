import axios from 'axios'
import { generateCuckooMessage } from "./GenerateCuckooMessage"

async function ofMessage(message){
    var botMessage = "";

    const filter = {
        searchBy: "name",
        propertyToSearch: message.split(' ')[0].toLowerCase(), 
        searchValue: message.substr(message.indexOf(" of ") + 4, message.length - message.indexOf(" of ") - 4).toLowerCase()
    }

    //add the x_mitre_ prefix to some of the properties
    if (filter.propertyToSearch === "detection" || filter.propertyToSearch === "platforms"){
        filter.propertyToSearch = "x_mitre_" + filter.propertyToSearch;
    }

    await axios.post('http://localhost:5000/attackPatterns/filter', filter)
        .then(res =>  {
            botMessage = filter.propertyToSearch + " of " + res.data[0][filter.searchBy] + ":\n\n" + res.data[0][filter.propertyToSearch];
            console.log(res.data);
        })
        .catch(() => botMessage = "Sorry, I didn\'t understand that.\nSay 'help' for list of commands.");

    return botMessage;
}

async function byMessage(message){
    const filter = {
        searchValue: message.split(' ')[0].toLowerCase(), 
        searchBy: message.substr(message.indexOf(" by ") + 4, message.length - message.indexOf(" by ") - 4).toLowerCase()
    }

    var botMessage = "Attack patterns including " + filter.searchValue + " in " + filter.searchBy + ":\n\n";

    await axios.post('http://localhost:5000/attackPatterns/filter', filter)
    .then(res =>  {
        //format all of the returned names of the attack patterns
        for (var i = 0; i < res.data.length; i++){
            botMessage += res.data[i].name + "\n";
        }
        console.log(res.data);
    })
    .catch(() => botMessage = "Sorry, I didn\'t understand that.\nSay 'help' for list of commands.");

    return botMessage;
}

//generate a bot message to answer the virus total request message
async function virusTotalMessage(message){
    const filter = {
        checkProperty: message.split(' ')[1].toLowerCase(),
        checkValue: message.split(' ')[2].toLowerCase()
    }

    //the property can only be hash or url
    if (!(filter.checkProperty === "url" || filter.checkProperty === "hash")){
        return "Unknown property, you can check url or hash";
    }

    //add the https prefix if it doesn't exist
    if (filter.checkProperty === "url" && !(filter.checkValue.includes("http://") || filter.checkValue.includes("https://"))){
        filter.checkValue = "https://" + filter.checkValue;
    }

    var botMessage = filter.checkValue + " report:\n";

    await axios.post('http://localhost:5000/attackPatterns/vt-check', filter)
    .then(res => {
        console.log(res.data);
        if (res.data.positives == undefined)
            botMessage = `Something wrong with the ${filter.checkProperty}.`
        else {
            //specify if the scanned url or file is safe or not
            botMessage += res.data.positives + "/" + res.data.total + " positives\n";
            if (res.data.positives > 4)
                botMessage += `The ${filter.checkProperty} is not safe.`;
            else if (res.data.positives > 0)
                botMessage += `The ${filter.checkProperty} might be safe.`;
            else 
                botMessage += `The ${filter.checkProperty} is safe.`;
        }
    })
    .catch(() => botMessage = `Something wrong with the ${filter.checkProperty}.`)

    return botMessage;
}

export function generateBotMsg(message) {
    if (message.toLowerCase() === "help" || message.toLowerCase() === "h"){
        //help message
        return "I can understand the following commands:\n" +
                "   1. help, h - shows a list of commands.\n" +
                "   2. clear - clears the chat.\n" +
                "   3. <property> of <attack pattern name> - shows the requested property of the requested attack pattern." +
                " (e.g. detection of VNC)\n" +
                "   4. <something to search> by <property> - shows a list of attack patterns in which the property includes the search value." +
                " (e.g. dll32 by description)\n" + 
                "   5. check <'url','hash'> <check value> - shows the report of a url or a hash in virustotal's database." +
                " (e.g. check url https://google.com)\n" + 
                "   6. cuckoo list - lists all of the existing tasks in cuckoo.\n" + 
                "   7. cuckoo delete <id> - deletes the task with the given id from cuckoo.\n" + 
                "   8. cuckoo report <id> - shows the report of a task with the given id.\n";
    }
    else if (message.toLowerCase().includes(" of ")){
        return ofMessage(message);
    }
    else if (message.toLowerCase().includes(" by ")){
        return byMessage(message);
    }
    else if (message.toLowerCase().split(' ')[0] === "check" && message.split(' ').length == 3){
        return virusTotalMessage(message);
    }
    else if (message.toLowerCase().split(' ')[0] === "cuckoo"){
        return generateCuckooMessage(message);
    }
    
    return "Sorry, I didn\'t understand that.\nSay 'help' for list of commands.";
}