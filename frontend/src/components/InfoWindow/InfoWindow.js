import React from "react"
import "bootstrap/dist/css/bootstrap.min.css"
import "./InfoWindow.css"

export default function InfoWindow(props) {
    if (!props.open) return null;

    return (
        <div>
            <div className="info-overlay"/>
            <div className="info-window">
                <button className="info-close-btn" onClick={props.onClose}>&times;</button>
                <div className="info">
                    <h2>Name</h2>
                    <p>{props.info.name}</p>
                    <h2>Id</h2>
                    <p>{props.info.id}</p>
                    <h2>Description</h2>
                    <p>{props.info.description}</p>
                    <h2>Platforms</h2>
                    <p>{props.info.x_mitre_platforms.join(', ')}</p>
                    <h2>Detection</h2>
                    <p>{props.info.x_mitre_detection}</p>
                    <h2>Phase name</h2>
                    <p>{props.info.phase_name}</p>
                </div>
            </div>
        </div>
    )
}