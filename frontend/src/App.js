import React from 'react'
import './App.css'
import AttackPatternList from './components/AttackPatternList/AttackPatternList';
import BotChat from './components/BotChat/BotChat';

function App() {
  return (
    <div>
      <h1 className="header">Welcome To The Cyber Defender</h1>
        <br />
      <AttackPatternList />
      <BotChat />
    </div>
  );
}

export default App;